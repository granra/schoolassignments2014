USE A8;

CREATE TABLE People (
       PID INT,
       pname VARCHAR(50),
       pgender CHAR(1),
       pheight FLOAT,
       PRIMARY KEY (PID)
);

CREATE TABLE Accounts (
       AID INT,
       PID INT,
       atype CHAR(1),
       adate DATE,
       asum FLOAT,
       PRIMARY KEY (AID),
       FOREIGN KEY (PID) REFERENCES People(PID)
);

CREATE TABLE AccountRecords (
       RID INT,
       AID INT,
       rdate DATE,
       ramount INT,
       rsum INT,
       PRIMARY KEY (RID, AID),
       FOREIGN KEY (AID) REFERENCES Accounts (AID)
);
