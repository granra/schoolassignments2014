CREATE DATABASE Assignment1;
USE Assignment1;

CREATE TABLE hobbies (
	id SERIAL,
	name VARCHAR(100),
	rank INTEGER,
	PRIMARY KEY(id)
);

CREATE TABLE activities (
	id SERIAL,
	name VARCHAR(100),
	hobby_id INTEGER,
	PRIMARY KEY(id),
	FOREIGN KEY(hobby_id) REFERENCES hobbies(id)
);

INSERT INTO hobbies (name, rank)
	VALUES ('Computers', 9);

INSERT INTO hobbies (name, rank)
	VALUES ('Music', 8);

INSERT INTO hobbies (name, rank)
	VALUES ('Photography', 6);

SELECT * FROM hobbies;

INSERT INTO activities (name, hobby_id)
	VALUES ('Listening to music', 2);

INSERT INTO activities (name, hobby_id)
	VALUES ('Programming', 1);

INSERT INTO activities (name, hobby_id)
	VALUES ('Playing guitar', 2);

SELECT * FROM activities;

SELECT
	a.name AS Activity,
	h.name AS Hobby
FROM hobbies AS h
	INNER JOIN activities AS a
	ON h.id = a.hobby_id
ORDER BY h.id;

SELECT a.name FROM activities AS a
RIGHT JOIN hobbies AS h
ON a.hobby_id = h.id
WHERE h.name = 'Music';

SELECT 
	h.name AS hobby,
	h.rank,
	count(a.id) AS nr_of_activities
FROM hobbies AS h
	LEFT OUTER JOIN activities AS a
	ON h.id = a.hobby_id
GROUP BY h.id;

DELETE FROM activities;

DELETE FROM hobbies;

DROP TABLE activities;

DROP TABLE hobbies;

DROP DATABASE Assignment1;
