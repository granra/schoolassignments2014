-----------------
SELECT 1 AS Query;

SELECT	 P.name, 
	 G.description AS gender
FROM 	 people P, 
	 gender G
WHERE 	 P.gender = G.gender
ORDER BY P.height;

-----------------
SELECT 2 AS Query;

SELECT 	 P.name
FROM	 people P,
	 results R
WHERE	 R.peopleid = P.id
GROUP BY P.id
HAVING 	 COUNT(DISTINCT R.competitionid) > 0;

-----------------
SELECT 3 AS Query;

SELECT 	count(DISTINCT peopleid) AS count
FROM 	results;

-----------------
SELECT 4 AS Query;

SELECT  P.id,
        P.name,
        MAX(R.result) AS rest,
        ROUND(CAST(MAX(R.result) - MIN(R.result) AS numeric), 2) AS range
FROM
        people P,
        sports S,
        results R
WHERE
        R.peopleid = P.id AND
        R.sportid = S.id AND
        S.name = 'High Jump'
GROUP BY P.id
HAVING COUNT(R.result) > 1;

-----------------
SELECT 5 AS Query;

SELECT	P.name,
	G.description AS gender
FROM	people P,
	gender G,
	sports S,
	results R
WHERE	R.sportid = S.id AND
	R.result = S.record AND
	R.peopleid = P.id AND
	G.gender = P.gender;

-----------------
SELECT 6 AS Query;

SELECT   P.id,
	 P.name,
	 count(P.id)
FROM     people P,
         sports S,
         results R
WHERE    R.sportid = S.id AND
         R.result = S.record AND
         R.peopleid = P.id
GROUP BY P.id
HAVING count(P.id) > 1;

-----------------
SELECT 7 AS Query;

SELECT 	 S.name AS sport,
		 COUNT(P.id) AS nr_of_record
FROM	 sports S,
		 people P,
		 results R
WHERE	 R.sportid = S.id AND
		 R.peopleid = P.id AND
		 R.result = S.record
GROUP BY S.id
HAVING 	 COUNT(P.id) > 1;

-----------------
SELECT 8 AS Query;

SELECT	DISTINCT
	P.id,
	P.name,
	G.description AS gender
FROM	people P,
	results R,
	competitions C,
	gender G
WHERE	R.competitionid = C.id AND
	R.peopleid = P.id AND
	P.gender = G.gender AND
	C.place = 'Kópasker' AND
	DATE_PART('year', C.held) = 2004;

-----------------
SELECT 9 AS Query;

SELECT	P.name,
	S.name AS sport,
	(ROUND((R.result / S.record) * 100) || '%') AS percentage
FROM	people P,
	results R,
	sports S
WHERE	R.peopleid = P.id AND
	R.sportid = S.id;

------------------
SELECT 10 AS Query;

SELECT * FROM people
WHERE name LIKE '% Friðgeirs%';
