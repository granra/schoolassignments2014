-- 1.
SELECT 	P.name, S.name AS sport, R.result
FROM 	results R, people P, sports S
WHERE 	R.peopleid = P.id
AND 	R.sportid = S.id
AND		NOT EXISTS(
	SELECT * FROM results
	WHERE 	sportid = R.sportid
	AND		result > R.result
);

-- 2.
SELECT 	DISTINCT P.name
FROM 	people P, results R, sports S
WHERE 	R.peopleid = P.id
AND	R.sportid = S.id
AND	R.result !=    (SELECT MIN(result)
			FROM results
		    	WHERE sportid = R.sportid);

-- 3.
SELECT 	name, height
FROM 	people
WHERE 	height >= 1.70
INTERSECT
SELECT 	name, height
FROM 	people
WHERE 	height <= 1.80;

SELECT 	name, height
FROM 	people
WHERE 	height >= 1.70
AND	height <= 1.80;

-- 4. Allir sem hafa ekki keppt í High Jump
SELECT 	name
FROM 	people
EXCEPT
SELECT 	P.name
FROM 	people P, sports S, results R
WHERE 	R.peopleid = P.id
AND 	R.sportid = S.id
AND 	S.name = 'High Jump';

-- 5. Allir sem hafa keppt í öllum íþróttum
SELECT 	P.name
FROM 	people P
WHERE 	NOT EXISTS (SELECT 	*
		    FROM 	sports S
		    WHERE 	NOT EXISTS (SELECT 	*
					    FROM 	results R
					    WHERE 	R.sportid = S.id
					    AND 	R.peopleid = P.id));
