SELECT 1 AS Query;

SELECT 	C.place, C.held
FROM 	competitions C FULL OUTER JOIN results R
ON 	R.competitionid = C.id
WHERE 	R.result is null;


SELECT 2 AS Query;

SELECT  COUNT(DISTINCT P.id)
FROM    people P
WHERE 	NOT EXISTS (SELECT *
		    FROM  results R, competitions C
		    WHERE R.competitionid = C.id
		    AND   R.peopleid = P.id);


SELECT 3 AS Query;

SELECT 	DISTINCT P.id, P.name
FROM 	people P, competitions C, results R
WHERE 	R.competitionid = C.id
AND 	R.peopleid = P.id
AND 	C.place = 'Kópasker'
UNION
SELECT 	P.id, P.name
FROM 	people P, results R, sports S
WHERE 	R.peopleid = P.id
AND 	R.sportid = S.id
AND 	R.result = S.record;


SELECT 4 AS Query;

SELECT	P.id, P.name
FROM    people P, results R, sports S
WHERE   R.peopleid = P.id
AND     R.sportid = S.id
AND     R.result = S.record
AND 	NOT EXISTS (SELECT *
		    FROM   results R2
		    WHERE  R2.sportid != R.sportid
		    AND    R2.peopleid = P.id);


SELECT 5 AS Query;

SELECT S.id,
	S.name,
	(ROUND(CAST(((AVG(R.result) / S.record) * 100) AS numeric), 2) || '%') AS avgperc
FROM sports S, results R
WHERE R.sportid = S.id
GROUP BY S.id, S.name;


SELECT 6 AS Query;

SELECT 	 S.id,
       	 S.name,
       	 (ROUND(CAST(((AVG(R.result) / S.record) * 100) AS numeric), 2) || '%') AS avgperc
FROM 	 sports S, results R
WHERE 	 R.sportid = S.id
GROUP BY S.id, S.name
HAVING   (AVG(R.result) / S.record) <= ALL(SELECT (AVG(R1.result)/S1.record)
					   FROM results R1, sports S1
					   WHERE R1.sportid = S1.id
					   GROUP BY S1.record);


SELECT 7 AS Query;

SELECT 	P.id,
	P.name,
	R.result,
	S.name AS sport,
	CASE WHEN R.result = S.record THEN 'yes'
	     ELSE 'no'
	END AS record
FROM 	people P, results R, sports S
WHERE 	R.peopleid = P.id
AND 	R.sportid = S.id
AND 	R.result = (SELECT MAX(R1.result)
		    FROM results R1
		    WHERE R1.sportid = S.id);


SELECT 8 AS Query;

SELECT COUNT(id)
FROM   (SELECT P.id, COUNT(DISTINCT C.id) AS nr
	FROM people P, results R, competitions C
	WHERE R.peopleid = P.id
	AND R.competitionid = C.id
	GROUP BY P.id) AS F
WHERE nr > 10;


SELECT 9 AS Query;

SELECT 	P.id, P.name
FROM 	people P
WHERE 	NOT EXISTS (SELECT *
		    FROM   sports S
		    WHERE  NOT EXISTS(SELECT *
				      FROM   results R
				      WHERE  R.sportid = S.id
				      AND    R.peopleid = P.id));


SELECT 10 AS Query;

SELECT P.id, P.name
FROM   people P
WHERE  NOT EXISTS (SELECT C.place
		   FROM   competitions C
		   WHERE  NOT EXISTS (SELECT *
				      FROM   results R, competitions C1
				      WHERE  R.competitionid = C1.id
				      AND    C1.place = C.place
				      AND    R.peopleid = P.id));
