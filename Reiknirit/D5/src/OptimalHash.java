import edu.princeton.cs.introcs.StdOut;

/**
 * Created by arnar on 10/9/14.
 */
public class OptimalHash
{
    public static void main(String[] args)
    {
        int M = 0, a = 0;
        char[] chars = {'S', 'E', 'A', 'R', 'C', 'H', 'X', 'M', 'P', 'L'};
        boolean collision = false;

        for (int i = chars.length; i < 100; ++i)
        {
            for (int j = 1; j < 10; ++j)
            {
                boolean[] arr = new boolean[i];
                collision = false;
                for (char c : chars)
                {
                    int hash = (j * c) % i;
                    if (arr[hash])
                    {
                        collision = true;
                        break;
                    }
                    arr[hash] = true;

                }
                if (!collision)
                {
                    M = i; a = j;
                    break;
                }
            }
            if (!collision) break;
        }

        StdOut.println("M = " + M + ", a = " + a);
    }
}
