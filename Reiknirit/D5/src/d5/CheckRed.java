package d5;

import edu.princeton.cs.introcs.*;
import edu.princeton.cs.algs4.*;

public class CheckRed 
{
	public static void main(String[] args) 
	{
		int N = 10000;
		for (int i = 0; i < 3; N *= 10, ++i) // Keyrir N = 10000, 100000, 1000000
		{
			double sum = 0;
			for (int k = 0; k < 100; ++k) // Keyrir 100 sinnum fyrir hvert N
			{
				RedBlackBST<Integer, Integer> tree = new RedBlackBST<Integer, Integer>();
				for (int l = 0; l < N; ++l) // Keyrir N sinnum til að setja N random stök í tréð
				{
					Integer key = StdRandom.uniform(Integer.MAX_VALUE);
					tree.put(key, key);
				}
				sum += tree.nrOfReds();
			}
			StdOut.println(N + ": " + sum/N + "%");
		}
	}
}