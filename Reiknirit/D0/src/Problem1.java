/**
 * Created by arnar on 8/19/14.
 */

import edu.princeton.cs.introcs.Draw;

import java.awt.*;

public class Problem1 {
    public static void main(String [] args)
    {
        Draw drawing = new Draw();
        drawing.circle(.5, .5, .5);

        drawing.setPenColor(Draw.BLUE);
        drawing.filledSquare(.5, .5, .35);

        drawing.setPenRadius(.01);
        drawing.setPenColor(Draw.YELLOW);
        drawing.ellipse(.5, .3, .2, .05);

        drawing.setPenColor(Draw.RED);
        drawing.setPenRadius(.02);
        drawing.line(.4, .4, .6, .4);
        drawing.line(.5, .3, .5, .7);

        drawing.setFont(new Font("inconsolata", Font.BOLD, 50));
        drawing.setPenColor(Draw.WHITE);
        drawing.text(.5, .2, "Hail satan");
    }
}