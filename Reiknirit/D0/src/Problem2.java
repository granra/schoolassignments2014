/**
 * Created by arnar on 8/19/14.
 */

import edu.princeton.cs.introcs.StdIn;

public class Problem2 {
    public static void main(String [] args)
    {
        int n = StdIn.readInt();

        int[] arr = new int[n];

        arr[0] = StdIn.readInt();

        int smallest = arr[0], largest = arr[0];

        for (int i = 1; i < n; ++i)
        {
            arr[i] = StdIn.readInt();

            if (arr[i] < smallest)
            {
                smallest = arr[i];
            }
            if (arr[i] > largest)
            {
                largest = arr[i];
            }
        }

        System.out.println(largest - smallest);
    }
}