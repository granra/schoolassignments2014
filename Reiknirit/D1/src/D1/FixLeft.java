/**
 * Created by arnar on 8/26/14.
 */

import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.StdOut;
import edu.princeton.cs.algs4.Stack;

public class FixLeft {
    public static void main(String [] args)
    {
        Stack<String> vals = new Stack<String>();

        while (!StdIn.isEmpty())
        {
            String s = StdIn.readString();

            if (s.equals(")"))
            {
                String latter = vals.pop(), op = vals.pop(), former = vals.pop();
                vals.push("( " + former + " " + op + " " + latter + " )");
            }
            else
            {
                vals.push(s);
            }
        }

        StdOut.println(vals.pop());
    }
}
