package s2;

import edu.princeton.cs.algs4.MergeX;
import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.StdOut;

import java.util.*;

/**
 * Created by arnar on 9/21/14.
 */
public class Brute {
    public static void main(String[] args) {
        int N = StdIn.readInt();
        Point[] arr = new Point[N];
        for (int i = 0; i < N; ++i) {
            arr[i] = new Point(StdIn.readInt(), StdIn.readInt());
        }

        MergeX.sort(arr);

        for (int p = 0; p < N; ++p) {
            for (int q = p + 1; q < N; ++q) {
                for (int r = q + 1; r < N; ++r) {
                    if (arr[p].slopeTo(arr[q]) == arr[q].slopeTo(arr[r])) {
                        for ( int s = r + 1; s<N ; ++s){
                            if (arr[q].slopeTo(arr[r]) == arr[r].slopeTo(arr[s])) {
                                Point[] ans = {arr[p], arr[q], arr[r], arr[s]};
                                MergeX.sort(ans);
                                StdOut.println(ans[0] + " -> " +
                                               ans[1] + " -> " +
                                               ans[2] + " -> " +
                                               ans[3]);
                            }
                        }
                    }
                }
            }
        }
    }
}
