package s2;

import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.StdOut;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by arnar on 9/22/14.
 */
public class Fast {
    public static void main(String[] args) {
        int N = StdIn.readInt();
        Point[] arr = new Point[N];

        for (int i = 0; i < N; ++i) {
            arr[i] = new Point(StdIn.readInt(), StdIn.readInt());
        }

        Arrays.sort(arr);

        Point[] sortedArr = new Point[N];

        for (int i = 0; i < N; ++i) {
            Point p = arr[i], lastPoint = null;

            System.arraycopy(arr, 0, sortedArr, 0, N);

            Arrays.sort(sortedArr, i, N, p.SLOPE_ORDER);

            double last = p.slopeTo(sortedArr[i]);

            int high = 0, low = 0;

            for (int q = i + 1; q < N; ++q) {
                double current = p.slopeTo(sortedArr[q]);

                if (last == current) {
                    ++high;
                }
                else {
                    if (high - low >= 2 && sortedArr[high] != lastPoint) {
                        lastPoint = sortedArr[high];

                        StdOut.print(p);

                        for (int l = low; l <= high; ++l) StdOut.print(" -> " + sortedArr[l]);

                        StdOut.print("\n");
                    }

                    low = q; high = q; last = current;
                }
            }

            if (high - low >= 2 && sortedArr[high] != lastPoint) {
                StdOut.print(p);

                for (int l = low; l <= high; ++l) StdOut.print(" -> " + sortedArr[l]);

                StdOut.print("\n");
            }
        }
    }
}
