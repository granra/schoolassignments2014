package s2;

import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.introcs.In;
import edu.princeton.cs.introcs.StdOut;
import edu.princeton.cs.introcs.StdRandom;

import java.util.Arrays;

public class Sigga {

    public static void Fast(Point[] arrayPoint) {
        int number = arrayPoint.length;
        Point last = null;

        //sortera array-ið og búa til nýtt array
        Arrays.sort(arrayPoint);
        Point[] sorted = new Point[number];

        for(int q = 0; q < number; q++)
        {
            Point point = arrayPoint[q];

            //afrita sorteraða arrayið yfir í nýja arrayið
            System.arraycopy(arrayPoint, 0, sorted, 0, sorted.length);
            //sortera nýja arrayið frá q uppí number eftir hallatölu
            Arrays.sort(sorted, q, number, point.SLOPE_ORDER);

            int low = 0;
            int high = 0;
            double lastSlope = point.slopeTo(sorted[q]);

            for(int r = q + 1; r < number; r++)
            {
                double currSlope = point.slopeTo(sorted[r]);

                //finna öll stök með sömu hallatölu til að prenta út
                if(currSlope == lastSlope){
                    high++;
                }
                else {
                    //ef það eru tvö eða fleiri stök með sömu hallatölu og engar tvítekningar
                    if((high - low >= 2) && (sorted[high] != last)){
                        last = sorted[high];

                        StdOut.print(point);

                        for(int s = low; s <= high; s++)
                        {
                            StdOut.print(" -> " + sorted[s]);
                        }
                        StdOut.println();
                    }
                    low = r;
                    high = r;
                    lastSlope = currSlope;
                }
            }
            //ef það eru tvö eða fleiri stök með sömu hallatölu og engar tvítekningar
            if((high - low >= 2) && (sorted[high] != last)){
                last = sorted[high];

                StdOut.print(point);

                for(int t = low; t <= high; t++)
                {
                    StdOut.print(" -> " + sorted[t]);
                }
                StdOut.println();
            }
        }
    }

    public static Point[] genPoints(int N) {
        Point[] arr = new Point[N];
        for (int i = 0; i < N; ++i) {
            arr[i] = new Point(StdRandom.uniform(100000), StdRandom.uniform(100000));
        }

        return arr;
    }

    public static void main(String[] args) {
        Point[] arr;

        // 150
        StdOut.println(150 + ":");
        arr = genPoints(150);
        Stopwatch stopwatch = new Stopwatch();
        Fast(arr);
        StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");

        // 200
        StdOut.println(200 + ":");
        arr = genPoints(200);
        stopwatch = new Stopwatch();
        Fast(arr);
        StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");

        // 300
        StdOut.println(300 + ":");
        arr = genPoints(300);
        stopwatch = new Stopwatch();
        Fast(arr);
        StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");

        for (int i = 400; i <= 12800; i *= 2) {
            StdOut.println(i + ":");
            arr = genPoints(i);
            stopwatch = new Stopwatch();
            Fast(arr);
            StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");
        }
    }
}