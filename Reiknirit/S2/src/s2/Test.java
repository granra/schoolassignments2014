package s2;

import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.introcs.StdOut;
import edu.princeton.cs.introcs.StdRandom;

import java.util.Arrays;

/**
 * Created by arnar on 9/26/14.
 */

public class Test {
    public static void Brute(Point[] arr) {
        int N = arr.length;
        Arrays.sort(arr);

        for (int p = 0; p < N; ++p) {
            for (int q = p + 1; q < N; ++q) {
                for (int r = q + 1; r < N; ++r) {
                    if (arr[p].slopeTo(arr[q]) == arr[q].slopeTo(arr[r])) {
                        for ( int s = r + 1; s<N ; ++s){
                            if (arr[q].slopeTo(arr[r]) == arr[r].slopeTo(arr[s])) {
                                Point[] ans = {arr[p], arr[q], arr[r], arr[s]};
                                Arrays.sort(ans);
                                /*StdOut.println(ans[0] + " -> " +
                                        ans[1] + " -> " +
                                        ans[2] + " -> " +
                                        ans[3]);*/
                            }
                        }
                    }
                }
            }
        }
    }

    public static void Fast(Point[] arr) {
        int N = arr.length;
        Arrays.sort(arr);

        Point[] sortedArr = new Point[N];

        for (int i = 0; i < N; ++i) {
            Point p = arr[i], lastPoint = null;

            System.arraycopy(arr, 0, sortedArr, 0, N);

            Arrays.sort(sortedArr, i, N, p.SLOPE_ORDER);

            double last = p.slopeTo(sortedArr[i]);

            int high = 0, low = 0;

            for (int q = i + 1; q < N; ++q) {
                double current = p.slopeTo(sortedArr[q]);

                if (last == current) {
                    ++high;
                }
                else {
                    if (high - low >= 2 && sortedArr[high] != lastPoint) {
                        lastPoint = sortedArr[high];

                        //StdOut.print(p);

                        for (int l = low; l <= high; ++l) StdOut.print(" -> " + sortedArr[l]);

                        //StdOut.print("\n");
                    }

                    low = q; high = q; last = current;
                }
            }

            if (high - low >= 2 && sortedArr[high] != lastPoint) {
                //StdOut.print(p);

                for (int l = low; l <= high; ++l) StdOut.print(" -> " + sortedArr[l]);

                //StdOut.print("\n");
            }
        }
    }

    public static Point[] genPoints(int N) {
        Point[] arr = new Point[N];
        for (int i = 0; i < N; ++i) {
            arr[i] = new Point(StdRandom.uniform(100000), StdRandom.uniform(100000));
        }

        return arr;
    }

    public static void main(String[] args) {
        Point[] arr;

        // 150
        StdOut.println(150 + ":");
        arr = genPoints(150);
        Stopwatch stopwatch = new Stopwatch();
        Brute(arr);
        StdOut.println("Brute: " + stopwatch.elapsedTime());

        stopwatch = new Stopwatch();
        Fast(arr);
        StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");

        // 200
        StdOut.println(200 + ":");
        arr = genPoints(200);
        stopwatch = new Stopwatch();
        Brute(arr);
        StdOut.println("Brute: " + stopwatch.elapsedTime());

        stopwatch = new Stopwatch();
        Fast(arr);
        StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");

        // 300
        StdOut.println(300 + ":");
        arr = genPoints(300);
        stopwatch = new Stopwatch();
        Brute(arr);
        StdOut.println("Brute: " + stopwatch.elapsedTime());

        stopwatch = new Stopwatch();
        Fast(arr);
        StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");

        for (int i = 400; i <= 12800; i *= 2) {
            StdOut.println(i + ":");
            arr = genPoints(i);
            stopwatch = new Stopwatch();
            Brute(arr);
            StdOut.println("Brute: " + stopwatch.elapsedTime());

            stopwatch = new Stopwatch();
            Fast(arr);
            StdOut.println("Fast: " + stopwatch.elapsedTime() + "\n");
        }
    }
}
