import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.Stopwatch;
import edu.princeton.cs.algs4.QuickFindUF;
import edu.princeton.cs.algs4.QuickUnionUF;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.introcs.StdRandom;
import edu.princeton.cs.introcs.StdOut;

public class DoublingTest {

    // This class should not be instantiated.
    private DoublingTest() { }

    public static double timeTrial(int n, int j) {
        QuickFindUF uf = new QuickFindUF(j);
        //QuickUnionUF uf = new QuickUnionUF(j);
        //WeightedQuickUnionUF uf = new WeightedQuickUnionUF(j);
        int k = (int) Math.floor(0.5 * j * (Math.log(n)));
        Stopwatch stopwatch = new Stopwatch();
        for (int i = 0; i < k; ++i) {
            int p = StdRandom.uniform(j);
            int q = StdRandom.uniform(j);
            if (!uf.connected(p, q)) uf.union(p, q);
        }

        return stopwatch.elapsedTime();
    }

    public static void main(String[] args) {
        StdOut.println("Starting value?");
        int N = StdIn.readInt();
        StdOut.println("How many iterations for each value of N?");
        int T = StdIn.readInt();

        double lastAverage = 0.0;

        for (int l = 0, j = N; l < 6; ++l, j *= 2) {
            double averageRun = 0.0;
            for (int a = 0; a < T; ++a) {
                averageRun += timeTrial(N, j);
            }

            StdOut.println("Average running time for " + j + " cases: " + (averageRun/T));
            if(lastAverage != 0) StdOut.println("Ratio for " + j + " cases: " + ((averageRun/T)/lastAverage));
            lastAverage = averageRun/T;
        }
    }
} 

