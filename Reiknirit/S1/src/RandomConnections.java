/**
 * Created by arnar on 9/9/14.
 */
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.introcs.StdRandom;

import java.util.ArrayList;

public class RandomConnections {
    public static Connection[] genConnections(int N) {
        ArrayList<Connection> arr = new ArrayList<Connection>();
        WeightedQuickUnionUF uf = new WeightedQuickUnionUF(N);
        int c = 0;
        while (uf.count() > 1) {
            int p = StdRandom.uniform(N);
            int q = StdRandom.uniform(N);
            arr.add(new Connection(p, q));
            uf.union(p, q);
            ++c;
        }

        return arr.toArray(new Connection[c]);
    }

    public static Connection[] genGridConnections(int M) {
        int size = M*M, c = 0;
        ArrayList<Connection> arr = new ArrayList<Connection>();
        WeightedQuickUnionUF uf = new WeightedQuickUnionUF(size);

        while (uf.count() > 1) {
            int p = StdRandom.uniform(size - 1);

            if (p - M >= 0 && !uf.connected(p, p - M)) {
                uf.union(p, p - M);
                arr.add(new Connection(p, p - M));
                ++c;
            }
            if (p + M < size && !uf.connected(p, p + M)) {
                uf.union(p, p + M);
                arr.add(new Connection(p, p + M));
                ++c;
            }
            if (p - 1 >= 0 && p % M > 0 && !uf.connected(p, p - 1)) {
                uf.union(p, p - 1);
                arr.add(new Connection(p, p - 1));
                ++c;
            }
            if (p + 1 < size && p % M < M - 1 && !uf.connected(p, p + 1)) {
                uf.union(p, p + 1);
                arr.add(new Connection(p, p + 1));
                ++c;
            }
        }

        return arr.toArray(new Connection[c]);
    }
}
