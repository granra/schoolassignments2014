import edu.princeton.cs.introcs.StdOut;

/**
 * Created by arnar on 9/9/14.
 */
public class ConnectionTest {
    public static void main(String[] args) {
        Connection[] arr = RandomConnections.genConnections(10);

        for (int i = 0; i < arr.length; ++i) {
            StdOut.println(arr[i].p() + " - " + arr[i].q());
        }
    }
}
