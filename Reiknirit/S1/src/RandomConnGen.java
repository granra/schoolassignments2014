import edu.princeton.cs.algs4.QuickFindUF;
import edu.princeton.cs.algs4.QuickUnionUF;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.StdOut;
import edu.princeton.cs.introcs.StdRandom;
import edu.princeton.cs.introcs.Stopwatch;

/**
 * Created by arnar on 9/9/14.
 */
public class RandomConnGen {

    public static double QuickFindTrial(int j, Connection[] arr) {
        QuickFindUF uf = new QuickFindUF(j);
        Stopwatch stopwatch = new Stopwatch();
        for (Connection conn : arr) {
            uf.union(conn.p(), conn.q());
        }

        return stopwatch.elapsedTime();
    }

    public static double QuickUnionTrial(int j, Connection[] arr) {
        QuickUnionUF uf = new QuickUnionUF(j);
        Stopwatch stopwatch = new Stopwatch();
        for (Connection conn : arr) {
            uf.union(conn.p(), conn.q());
        }

        return stopwatch.elapsedTime();
    }

    public static double WeightedQUTrial(int j, Connection[] arr) {
        WeightedQuickUnionUF uf = new WeightedQuickUnionUF(j);
        Stopwatch stopwatch = new Stopwatch();
        for (Connection conn : arr) {
            uf.union(conn.p(), conn.q());
        }

        return stopwatch.elapsedTime();
    }

    public static double WQUPCTrial(int j, Connection[] arr) {
        WeightedQuickUnionPathCompressionUF uf = new WeightedQuickUnionPathCompressionUF(j);
        Stopwatch stopwatch = new Stopwatch();
        for (Connection conn : arr) {
            uf.union(conn.p(), conn.q());
        }

        return stopwatch.elapsedTime();
    }

    public static double HQUTrial(int j, Connection[] arr) {
        WeightedQuickUnionByHeightUF uf = new WeightedQuickUnionByHeightUF(j);
        Stopwatch stopwatch = new Stopwatch();
        for (Connection conn : arr) {
            uf.union(conn.p(), conn.q());
        }

        return stopwatch.elapsedTime();
    }

    public static void main(String[] args) {
        StdOut.println("Starting value?");
        int N = StdIn.readInt();
        int T = 10;

        double AverageRun1, AverageRun2, AverageRun3;
        for (int l = 0, j = N; l < 5; ++l, j *= 2) {
            StdOut.println(j + ":");
            //Connection[] arr = RandomConnections.genGridConnections((int)Math.sqrt(j));
            Connection[] arr = RandomConnections.genConnections(j);

            AverageRun1 = 0.0; AverageRun2 = 0.0; AverageRun3 = 0.0;
            for (int a = 0; a < T; ++a) {
                AverageRun1 += QuickUnionTrial(j, arr) / QuickFindTrial(j, arr);
                AverageRun2 += WQUPCTrial(j, arr) / WeightedQUTrial(j, arr);
                AverageRun3 += WeightedQUTrial(j, arr) / HQUTrial(j, arr);
            }
            StdOut.println("QuickFind and QuickUnion: " + (AverageRun1/T));
            StdOut.println("WeightedQuickUnion and WeightedQuickUnionPathComepression: " + (AverageRun2/T));
            StdOut.println("HeightedQuickUnion and WeightedQuickUnion: " + (AverageRun3/T));
            StdOut.println("");
        }
    }
}
