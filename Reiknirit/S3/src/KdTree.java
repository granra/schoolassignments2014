
/*************************************************************************
 *************************************************************************/

import java.util.ArrayList;
import java.util.Collections;

import edu.princeton.cs.algs4.*;
import edu.princeton.cs.introcs.*;

public class KdTree {

    private PointNode root;

    private static class PointNode {
        private Point2D point;
        private RectHV rect;
        private PointNode left, right;

        public PointNode(Point2D point) {
            this.point = point;
            this.rect = new RectHV(0, 0, 1, 1);
        }
    }
    // construct an empty set of points
    public KdTree() {
        root = null;
    }

    // is the set empty?
    public boolean isEmpty() {
        return root == null;
    }

    // number of points in the set
    public int size() {
        return size(root);
    }

    private int size(PointNode node) {
        if (node != null) {
            return 1 + size(node.left) + size(node.right);
        }
        return 0;
    }

    // add the point p to the set (if it is not already in the set)
    public void insert(Point2D p) {
        root = insert(root, p, true);
    }

    private PointNode insert(PointNode node, Point2D p, boolean type) {
        if (node == null) return new PointNode(p);

        if (p.equals(node.point)) return node;

        if (type) { // true == Vertical
            if (p.x() < node.point.x()) {
                node.left = insert(node.left, p, !type);
                node.left.rect = new RectHV(node.rect.xmin(), node.rect.ymin(), node.point.x(), node.rect.ymax());
            }
            else { // p.x() >= node.point.x()
                node.right = insert(node.right, p, !type);
                node.right.rect = new RectHV(node.point.x(), node.rect.ymin(), node.rect.xmax(), node.rect.ymax());
            }
        }
        else { // false == Horizontal
            if (p.y() < node.point.y()) {
                node.left = insert(node.left, p, !type);
                node.left.rect = new RectHV(node.rect.xmin(), node.rect.ymin(), node.rect.xmax(), node.point.y());
            }
            else { // p.y() >= node.point.y()
                node.right = insert(node.right, p, !type);
                node.right.rect = new RectHV(node.rect.xmin(), node.point.y(), node.rect.xmax(), node.rect.ymax());
            }
        }
        return node;
    }

    // does the set contain the point p?
    public boolean contains(Point2D p) {
        return contains(root, p, true);
    }

    private boolean contains(PointNode node, Point2D p, boolean type) {
        if (node != null) {
            if (p.equals(node.point)) return true;

            if (type) { // true == Vertical
                if (p.x() < node.point.x())       return contains(node.left, p, !type);
                else if (p.x() >= node.point.x()) return contains(node.right, p, !type);
            }
            else { // false == Horizontal
                if (p.y() < node.point.y())       return contains(node.left, p, !type);
                else if (p.y() >= node.point.y()) return contains(node.right, p, !type);
            }
        }
        return false;
    }

    // draw all of the points to standard draw
    public void draw() {
        draw(root, true);
    }

    private void draw(PointNode node, boolean type) {
        if (node != null) {
            if (type) { // true == Vertical
                StdDraw.setPenColor(StdDraw.RED);
                StdDraw.line(node.point.x(), node.rect.ymin(), node.point.x(), node.rect.ymax());
            }
            else { // false == Horizontal
                StdDraw.setPenColor(StdDraw.BLUE);
                StdDraw.line(node.rect.xmin(), node.point.y(), node.rect.xmax(), node.point.y());
            }
            StdDraw.setPenColor(StdDraw.BLACK);

            StdDraw.filledCircle(node.point.x(), node.point.y(), 0.003);

            draw(node.left, !type);
            draw(node.right, !type);
        }
    }

    // all points in the set that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        SET<Point2D> set = new SET<Point2D>();
        range(root, rect, set, true);

        return set;
    }

    private void range(PointNode node, RectHV rect, SET<Point2D> set, boolean type) {
        if (node != null) {
            if (rect.contains(node.point)) set.add(node.point);

            if (rect.intersects(node.rect)) {
                range(node.left, rect, set, !type);
                range(node.right, rect, set, !type);
            }
        }
    }

    // a nearest neighbor in the set to p; null if set is empty
    public Point2D nearest(Point2D p) {
        return nearest(root, root, p, true);
    }

    private Point2D nearest(PointNode node, PointNode shortest, Point2D p, boolean type) {
        if (node != null) {
            if (p.distanceTo(node.point) < p.distanceTo(shortest.point))
                shortest = node;

            if (type) { // true == Vertical
                if (p.x() < node.point.x())
                    return nearest(node.left, shortest, p, !type);
                else
                    return nearest(node.right, shortest, p, !type);
            }
            else { // false == Horizontal
                if (p.y() < node.point.y())
                    return nearest(node.left, shortest, p, !type);
                else
                    return nearest(node.right, shortest, p, !type);
            }
        }

        return shortest.point;
    }

    /*******************************************************************************
     * Test client
     ******************************************************************************/
    public static void main(String[] args) {
        In in = new In("/home/arnar/School Assignments/Reiknirit/S3/input.txt");
        Out out = new Out();
        int N = in.readInt(), C = in.readInt(), T = 50;
        Point2D[] queries = new Point2D[C];
        KdTree tree = new KdTree();
        out.printf("Inserting %d points into tree\n", N);
        for (int i = 0; i < N; i++) {
            tree.insert(new Point2D(in.readDouble(), in.readDouble()));
        }
        out.printf("tree.size(): %d\n", tree.size());
        out.printf("Testing `nearest` method, querying %d points\n", C);

        for (int i = 0; i < C; i++) {
            queries[i] = new Point2D(in.readDouble(), in.readDouble());
            out.printf("%s: %s\n", queries[i], tree.nearest(queries[i]));
        }
        for (int i = 0; i < T; i++) {
            for (int j = 0; j < C; j++) {
                tree.nearest(queries[j]);
            }
        }
    }
}
