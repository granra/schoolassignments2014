import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.StdOut;
/**
 * Created by arnar on 8/31/14.
 */
public class main {
    public static void main (String [] args)
    {
        QuickUnionWeighted find = new QuickUnionWeighted(10);
        int p, q;

        while (!StdIn.isEmpty())
        {
            p = StdIn.readInt();
            q = StdIn.readInt();

            find.union(p, q);
        }

        StdOut.print(find);
    }
}
