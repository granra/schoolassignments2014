/**
 * Created by arnar on 8/31/14.
 */
public class QuickUnion {
    private int[] id;

    public QuickUnion (int N)
    {
        id = new int[N];
        for (int i = 0; i < N; ++i)
        {
            id[i] = i;
        }
    }

    private int root (int i)
    {
        while (i != id[i])
        {
            i = id[i];
        }
        return i;
    }

    public boolean connected (int p, int q)
    {
        return root(p) == root(q);
    }

    public void union (int p, int q)
    {
        int i = root(p);
        int j = root(q);
        id[i] = j;
    }

    public String toString ()
    {
        String ret = new String();
        for (int i = 0; i < id.length; ++i)
        {
            ret += id[i] + " ";
        }
        return ret;
    }
}
