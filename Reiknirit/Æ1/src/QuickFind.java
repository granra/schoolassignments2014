/**
 * Created by arnar on 8/31/14.
 */
public class QuickFind {
    private int[] id;

    public QuickFind (int N)
    {
        id = new int[N];
        for (int i = 0; i < N; ++i)
        {
            id[i] = i;
        }
    }

    public boolean connected (int p, int q)
    {
        return id[p] == id[q];
    }

    public void union (int p, int q)
    {
        int pid = id[p];
        int qid = id[q];

        for (int i = 0; i < id.length; ++i)
        {
            if (id[i] == pid)
            {
                id[i] = qid;
            }
        }
    }

    public String toString ()
    {
        String ret = new String();
        for (int i = 0; i < id.length; ++i)
        {
            ret += id[i] + " ";
        }
        return ret;
    }
}
